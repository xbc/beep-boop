🎥 beep boop
YOU WILL NEED:
- Node.js v12.0.0+ (https://nodejs.org/)
    - In my experience, older versions may work, but aren't supported.
- A Discord bot account (https://discordpy.readthedocs.io/en/v1.4.1/discord.html)
    - You will need the token. DO NOT SHARE THIS!
    - Permissions required:
        - View Channels
        - Send Messages
        - Embed Links
        - Add Reactions
        - Attach Files
- Read/write access to files
    - In other words, a host that isn't Heroku.

HOW TO INSTALL (windows (10))
1. Open ".env" in a text editor, e.g. Notepad.
2. Replace "TOKEN HERE" with your bot's token (check previous section)
3. While holding Shift, right-click on an empty space within beep boop's folder.
4. Click "Open command prompt here" (or similar)
5. Type "npm install" (without quotes) then press enter.
6. Type "node app.js" (again, without quotes) then press enter.
- To exit the bot, close the command prompt.
7. In a channel beep boop can see, type "m!help".

HOW TO INSTALL (anything else)
1. idk, look on google maybe?

EMOJI:
Beep boop may react with emoji for chain-related actions. Here are the emoji used, and their meanings:
👌 - message seen and being acted upon
😒 - missing arguments (or something went wrong). Try typing "m!help".
✔ - command ran successfully. ~~cue r/BritishSuccess~~
