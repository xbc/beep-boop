require("dotenv").config();

const MC = require('purpl-markov-chain'),
    {Client, Intents, MessageAttachment, MessageEmbed} = require("js"),
    {join} = require("path"),
    {readFileSync, writeFile} = require("fs"),
    fetch = require("node-fetch")

let bored = new MC(JSON.parse(readFileSync(join(__dirname, "markov", "backup.bored.json"), "utf8"))),
    rq = new MC(JSON.parse(readFileSync(join(__dirname, "markov", "backup.rq.json"), "utf8"))),
    client = new Client({
        shards: "auto",
        disableMentions: "all",
        ws: {intents: Intents.NON_PRIVILEGED}
    }),  // https://stackoverflow.com/a/46720712
    sleep = (ms) => {
        return new Promise(res => setTimeout(res, ms));
    }

client.on('ready', () => {
  console.log(`Ready! ${client.user.tag} / ${client.user.id}`);
});

client.on('message', message => {
  let args = message.content.slice(0).trim().split(/ +/g);
  
  switch(args.shift().toLowerCase()) {
    case "m!help":
      message.channel.send(new MessageEmbed().setTitle("🚧 list of commands").setDescription([
        "**m!generate**\* - generate an idea/question.",
        "**m!start**\* - add items to a chain. you will have to backup manually.",
        "**m!backup** - backup the chains, if you added some items.","**m!ping** - test if the bot's working.",
        //"**m!owoify** - rawr xd uwuuwuwuwuwuwuwuwuuwwu"
        "**m!owoify** - owo's owoify command but better. just add text!"
      ]).setFooter("* add \"rq\" or \"bored\", depending on which chain you want to use")) 
      break;
    case "m!ping":
      //message.channel.send("pong");
      message.reply("happy?")
      break;
      
    case "m!start":
      message.react("👌");
      let i = 0,
        type = args.shift();
      if(!type || !(type == "bored" || type == "rq")) return message.react("😒")
      switch(type) {
        case "bored":
          // original code
          for (i = 0; i < 25; i++) {
            fetch("https://kylebob.com/get.php?category=&search=", {method: 'GET'})
              .then(res => res.json())
              .then(json => {
                bored.update(json["thing"].replace("&#39;", "'"))
              })
              .catch(err => { message.react("😒"); console.error(err) });
            sleep(2500); // make this less spammy.
          } 
        case "rq":
          for (i = 0; i < 25; i++) {
            fetch("https://www.conversationstarters.com/random.php", {method: 'GET'})
              //.then(res => res.json())
            .then(res => res.text())
              .then(txt => {
                rq.update(txt.replace("<img src=bullet.gif width=17 height=16>", ""))
              }).catch(err => { message.react("😒"); console.error(err) });
            sleep(2750); // make this less spammy.
          } 
      }
      message.react("✔")
      break;
      
    case "m!backup": case "m!save": 
      writeFile(join(__dirname, "markov", "backup.bored.json"), JSON.stringify(bored.toJSON()), "utf8", (err) => {
              if (err) console.log(err);
      })
      writeFile(join(__dirname, "markov", "backup.rq.json"), JSON.stringify(rq.toJSON()), "utf8", (err) => {
              if (err) console.log(err);
      })
      message.react("✔")
      break;
      
    case "m!generate":
      let gentype = args.shift() // <img src=bullet.gif width=17 height=16>
      if(!gentype) {return message.react("😒"); break}
      switch (gentype) {
        case "bored":
          let result = bored.generate();
          if(result.length > 255) {
            message.channel.send(new MessageEmbed().setColor([255,255,254]).setDescription("**" + result + "**"));
          } else {
            message.channel.send(new MessageEmbed().setColor([255,255,254]).setTitle(result));
          }
          break;
        case "rq":
          let rsy = rq.generate();
          if(rsy.length > 255) {
            message.channel.send(new MessageEmbed().setColor([255,255,254]).setDescription("**" + rsy + "**"));
          } else {
            message.channel.send(new MessageEmbed().setColor([255,255,254]).setTitle(rsy));
          }
          break;
        default: return message.react("😒"); break
      } break;
      
    case "m!owo": case "m!uwu": case "m!owoify":
      // hewwo -> <3 hewwo, fwendo
      if(!args[0]) {return message.react("😒"); break}
      // jk. beep boop's more original than that.
      // this is some code i wrote earlier. good time to use it!
      let owo = require("@zuzak/owo"),
          text = args.join(" "),
          i = 0,
          max = 20000;

      for(i = 0; text.length < max; i++) {
          text = owo(text);
      }
      //console.log(`${text.length} (after ${int} iterations)`);  
      // attaching as a file, because of how large this is inevitably going to be
      message.channel.send(new MessageEmbed() // https://stackoverflow.com/a/2901298
                            .setTitle(`${text.length.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} characters (after ${int.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} iterations)`)
                           .attachFiles([new MessageAttachment(Buffer.from(text), "owo.txt")])
                           .setColor([255,255,254])
                          )
  }
});

// Log our bot in using the token from https://discordapp.com/developers/applications/me
console.log("Logging in...")
client.login(process.env.TOKEN);
